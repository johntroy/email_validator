package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * 
 * @author troya  991-530-754
 *
 */
public class EmailValidatorTest {
	
	
	@Test
	public void testisValidEmailRegular() {
		assertTrue("Invalid case chars", EmailValidator.isValid("troyadrian36@gmail.com"));
	}
	
	@Test
	public void testisValidEmailBoundaryIn() {
		assertTrue("Invalid case chars", EmailValidator.isValid("troyadrian36@gmail.com"));
	}
	
	@Test
	public void testisValidEmailException() {
		assertFalse("Invalid case chars", EmailValidator.isValid("<@troyadrian36@gmail.com"));
	}
	
	@Test
	public void testisValidBoundaryOut() {
		assertFalse("Invalid case chars", EmailValidator.isValid("<troyadrian36@gmail.com>"));
	}
	
	
	
	
	
	@Test
	public void testhasOneSymbolRegular() {
		assertTrue("Invalid case chars", EmailValidator.hasAtleastOneSymbol("troyadrian36@gmail.com"));
	}
	
	
	@Test
	public void testhasOneSymbolBoundaryIn() {
		assertTrue("Invalid case chars", EmailValidator.hasAtleastOneSymbol("troyadrian36@gmail.com"));
	}
	
	@Test
	public void testhasOneSymbolException() {
		assertFalse("Invalid case chars", EmailValidator.hasAtleastOneSymbol("troyadrian36@@gmail.com"));
	}
	
	@Test
	public void testhasOneSymbolBoundaryOut() {
		assertFalse("Invalid case chars", EmailValidator.hasAtleastOneSymbol("troyadrian36@@@gmail.com"));
	}
	
	
	
	
	
	
	@Test
	public void testhasAlphaCharacterRegular() {
		assertTrue("Invalid case chars", EmailValidator.hasAtLeastThreeAlphaCharsAccount("troy1adrian36@gmail.com"));
	}
	
	@Test
	public void testhasAlphaCharacterBoundaryIn() {
		assertTrue("Invalid case chars", EmailValidator.hasAtLeastThreeAlphaCharsAccount("troy1adrian36@gmail.com"));
	}
	
	@Test
	public void testhasAlphaCharacterException() {
		assertFalse("Invalid case chars", EmailValidator.hasAtLeastThreeAlphaCharsAccount("troy?#111adrian36@gmail.com"));
	}
	
	@Test
	public void testhasAlphaCharacterBoundaryOut() {
		assertFalse("Invalid case chars", EmailValidator.hasAtLeastThreeAlphaCharsAccount("troy?#11adrian36@gmail.com"));
	}
	
	
	
	
	
	@Test
	public void testhasAlphaCharacterforDomainRegular() {
		assertTrue("Invalid case chars", EmailValidator.hasAtLeastThreeAlphaCharsDomain("troyadrian36@gm1ail.com"));
	}
	
	
	@Test
	public void testhasAlphaCharacterforDomainBoundaryIn() {
		assertTrue("Invalid case chars", EmailValidator.hasAtLeastThreeAlphaCharsDomain("troyadrian36@gm1ail.com"));
	}
	
	
	@Test
	public void testhasAlphaCharacterforDomainException() {
		assertFalse("Invalid case chars", EmailValidator.hasAtLeastThreeAlphaCharsDomain("troyadrian36@gm$11ail.com"));
	}
	
	
	@Test
	public void testhasAlphaCharacterforDomainBoundaryOut() {
		assertFalse("Invalid case chars", EmailValidator.hasAtLeastThreeAlphaCharsDomain("troyadrian36@gm$11ail.com"));
	}
	

	
	@Test
	public void testhasAtleastTwoAlphaCharactersRegular() {
		assertTrue("Invalid case chars", EmailValidator.hasAtLeastTwoAlphaCharsExtension("troyadrian36@gmail.co2m"));
	}
	
	@Test
	public void testhasAtleastTwoAlphaCharactersBoundaryIn() {
		assertTrue("Invalid case chars", EmailValidator.hasAtLeastTwoAlphaCharsExtension("troyadrian36@gmail.co2m"));
	}
	
	
	@Test
	public void testhasAtleastTwoAlphaCharactersException() {
		assertFalse("Invalid case chars", EmailValidator.hasAtLeastTwoAlphaCharsExtension("troyadrian36@gmail.co$112m"));
	}
	
	
	@Test
	public void testhasAtleastTwoAlphaCharactersBoundaryOut() {
		assertFalse("Invalid case chars", EmailValidator.hasAtLeastTwoAlphaCharsExtension("troyadrian36@gmail.co$112m"));
	}
	
	
	


}
