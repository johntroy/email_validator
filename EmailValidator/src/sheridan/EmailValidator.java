package sheridan;


public class EmailValidator {
	

	    public static boolean isValid(final String email) {
        return email.matches("^[a-zA-Z0-9+_.-]+@[a-zA-Z.-]+$");
    }
	   	
	    
	    public static boolean hasAtleastOneSymbol(final String email) {
	    	  return email.matches("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$");
	    }
	    
	    
	    public static boolean hasAtLeastThreeAlphaCharsAccount(final String email) {
	    	   return email.matches("^[a-zA-Z0-9+_.-]+@[a-zA-Z.-]+$");
	    }
	    
	    
	    public static boolean hasAtLeastThreeAlphaCharsDomain(final String email) {
	    	 return email.matches("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$");
	    }
	    
	    
	    public static boolean hasAtLeastTwoAlphaCharsExtension(final String email) {
	  return email.matches("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$");
	    }
	
	
}
